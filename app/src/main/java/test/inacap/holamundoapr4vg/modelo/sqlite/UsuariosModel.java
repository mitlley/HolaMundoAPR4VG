package test.inacap.holamundoapr4vg.modelo.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by mitlley on 01-09-17.
 */

public class UsuariosModel {
    private HolaMundoDBHelper dbHelper;

    public UsuariosModel(Context con){
        this.dbHelper = new HolaMundoDBHelper(con);
    }

    public void crearUsuario(ContentValues usuario){
        // Crear el usuario en la base de datos
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(HolaMundoDBContract.HolaMundoDBUsuarios.TABLE_NAME, null, usuario);
    }

    public ContentValues obtenerUsuarioPorUsername(String username){
        // Solicitar una conexion a la base de datos
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        // Que columnas necesitamos
        String[] projection = {
                HolaMundoDBContract.HolaMundoDBUsuarios._ID,
                HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME,
                HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD
        };

        // Como filtramos
        String selection = HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME + " = ? LIMIT 1";

        // Parametros
        String[] selectionArgs = { username };

        // Pedimos los datos
        Cursor cursor = db.query(
                HolaMundoDBContract.HolaMundoDBUsuarios.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null, null, null
        );


        if(cursor.getCount() == 0){
            return null;
        }

        // Recorrer los datos
        cursor.moveToFirst();

        String cursor_username = cursor.getString(cursor.getColumnIndex(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME));
        String cursor_password = cursor.getString(cursor.getColumnIndex(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD));

        // Crear el objeto a devolver
        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME, cursor_username);
        usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD, cursor_password);

        return usuario;
    }
}



















