package test.inacap.holamundoapr4vg.vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.holamundoapr4vg.MainActivity;
import test.inacap.holamundoapr4vg.R;
import test.inacap.holamundoapr4vg.modelo.sqlite.HolaMundoDBContract;
import test.inacap.holamundoapr4vg.vista.fragmentos.BienvenidaFragment;
import test.inacap.holamundoapr4vg.vista.fragmentos.MapaFragment;
import test.inacap.holamundoapr4vg.vista.fragmentos.SegundoFragment;

public class HomeDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MapaFragment.OnFragmentInteractionListener, SegundoFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);

        //Llamamos a la barra de herramientas (barra superior)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Boton flotante, boton que esta en la esquina inferior derecha
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        // Ponemos el boton en escucha de un click
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Mensaje temporal numero 2
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Componente que permite la existencia del panel lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Llamamos al panel lateral
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Ponemos el panel lateral en escucha
        navigationView.setNavigationItemSelectedListener(this);

        // Llamar a nuestro TextView para mostrar el nombre de usuario
        // Debemos llegar a la cabecera del panel lateral

        View cabecera = navigationView.getHeaderView(0);
        TextView tvUsername = (TextView) cabecera.findViewById(R.id.tvUsername);

        // Tomar el nombre de usuario de las preferencias compartidas
        SharedPreferences sesion = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_SESION, Context.MODE_PRIVATE); // sesiones.xml
        // Tomamos el nombre de usuario (username
        String username = sesion.getString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME, "");


        // Mostrar el dato en el TextView
        tvUsername.setText(username);
    }

    @Override
    public void onBackPressed() {
        // Sobreescribimos el comportamiento del boton 'atras'
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Si el panel lateral esta abierto
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            // Cerramos el panel lateral
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // De lo contrario, actuamos de forma normal
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Manejamos la seleccion de una opcion en el
        // menu de la barra de herramientas (barra superior=


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout){
            // Manejamos el cierre de sesion
            SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_SESION, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sesiones.edit();

            editor.putString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME, "");
            editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, false);
            editor.commit();

            Intent i = new Intent(HomeDrawerActivity.this, MainActivity.class);
            startActivity(i);
            finish();

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Maneja lo que pasara cuando la persona
        // seleccione un item del panel lateral

        // Discriminando por el nombre (id) del item
        int id = item.getItemId();

        // Preparar el fragmento a mostrar
        Fragment fragmento = null;

        if (id == R.id.nav_camera) {
            // Handle the camera action

            fragmento = new SegundoFragment();
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_hola){

            fragmento = new MapaFragment();

        }

        if(fragmento != null){
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.flContent, fragmento).commit();
        }


        // Una vez que se ejecuta la accion, se cierra el panel lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String fragmentName, String action) {

        if(fragmentName.equals("BienvenidaFragment")){
            if(action.equals("CONTADOR")){
                Toast.makeText(getApplicationContext(), "El contador cambio", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
