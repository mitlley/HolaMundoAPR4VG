package test.inacap.holamundoapr4vg.vista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import test.inacap.holamundoapr4vg.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
